/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package currencyconverter;

/**
 *
 * @author Lasse
 */


// Retrieves the SOAP interface
import currencyconverter.webservice.CurrencyConvertor; 
// For communicating with the server
import currencyconverter.webservice.CurrencyConvertorSoap; 
// Enum for currency types
import currencyconverter.webservice.Currency;

public class CurrencyConverter {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        CurrencyConvertor cc = new CurrencyConvertor();
        // Gets an implementation of the interface we can use
        // to contact the web service.
        CurrencyConvertorSoap ccs = cc.getCurrencyConvertorSoap();
        // Send the SOAP request to the server and get the result from the web service
        double conversionRate = ccs.conversionRate(Currency.SEK, Currency.NOK);
        System.out.println("1SEK is worth NOK" + conversionRate );
    }
    
}
