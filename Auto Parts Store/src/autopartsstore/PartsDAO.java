/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package autopartsstore;

import org.orm.*;
import org.hibernate.Query;
import org.hibernate.LockMode;
import java.util.List;

public class PartsDAO {
	public static Parts loadPartsByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = autopartsstore.AutoPartsStorePersistentManager.instance().getSession();
			return loadPartsByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parts getPartsByORMID(int ID) throws PersistentException {
		try {
			PersistentSession session = autopartsstore.AutoPartsStorePersistentManager.instance().getSession();
			return getPartsByORMID(session, ID);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parts loadPartsByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = autopartsstore.AutoPartsStorePersistentManager.instance().getSession();
			return loadPartsByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parts getPartsByORMID(int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = autopartsstore.AutoPartsStorePersistentManager.instance().getSession();
			return getPartsByORMID(session, ID, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parts loadPartsByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (Parts) session.load(autopartsstore.Parts.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parts getPartsByORMID(PersistentSession session, int ID) throws PersistentException {
		try {
			return (Parts) session.get(autopartsstore.Parts.class, new Integer(ID));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parts loadPartsByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Parts) session.load(autopartsstore.Parts.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parts getPartsByORMID(PersistentSession session, int ID, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			return (Parts) session.get(autopartsstore.Parts.class, new Integer(ID), lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryParts(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = autopartsstore.AutoPartsStorePersistentManager.instance().getSession();
			return queryParts(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryParts(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = autopartsstore.AutoPartsStorePersistentManager.instance().getSession();
			return queryParts(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parts[] listPartsByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = autopartsstore.AutoPartsStorePersistentManager.instance().getSession();
			return listPartsByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parts[] listPartsByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = autopartsstore.AutoPartsStorePersistentManager.instance().getSession();
			return listPartsByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryParts(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From autopartsstore.Parts as Parts");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static List queryParts(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From autopartsstore.Parts as Parts");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Parts", lockMode);
			return query.list();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parts[] listPartsByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		try {
			List list = queryParts(session, condition, orderBy);
			return (Parts[]) list.toArray(new Parts[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parts[] listPartsByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			List list = queryParts(session, condition, orderBy, lockMode);
			return (Parts[]) list.toArray(new Parts[list.size()]);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parts loadPartsByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = autopartsstore.AutoPartsStorePersistentManager.instance().getSession();
			return loadPartsByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parts loadPartsByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = autopartsstore.AutoPartsStorePersistentManager.instance().getSession();
			return loadPartsByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parts loadPartsByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		Parts[] partses = listPartsByQuery(session, condition, orderBy);
		if (partses != null && partses.length > 0)
			return partses[0];
		else
			return null;
	}
	
	public static Parts loadPartsByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		Parts[] partses = listPartsByQuery(session, condition, orderBy, lockMode);
		if (partses != null && partses.length > 0)
			return partses[0];
		else
			return null;
	}
	
	public static java.util.Iterator iteratePartsByQuery(String condition, String orderBy) throws PersistentException {
		try {
			PersistentSession session = autopartsstore.AutoPartsStorePersistentManager.instance().getSession();
			return iteratePartsByQuery(session, condition, orderBy);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iteratePartsByQuery(String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		try {
			PersistentSession session = autopartsstore.AutoPartsStorePersistentManager.instance().getSession();
			return iteratePartsByQuery(session, condition, orderBy, lockMode);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iteratePartsByQuery(PersistentSession session, String condition, String orderBy) throws PersistentException {
		StringBuffer sb = new StringBuffer("From autopartsstore.Parts as Parts");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static java.util.Iterator iteratePartsByQuery(PersistentSession session, String condition, String orderBy, org.hibernate.LockMode lockMode) throws PersistentException {
		StringBuffer sb = new StringBuffer("From autopartsstore.Parts as Parts");
		if (condition != null)
			sb.append(" Where ").append(condition);
		if (orderBy != null)
			sb.append(" Order By ").append(orderBy);
		try {
			Query query = session.createQuery(sb.toString());
			query.setLockMode("Parts", lockMode);
			return query.iterate();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parts createParts() {
		return new autopartsstore.Parts();
	}
	
	public static boolean save(autopartsstore.Parts parts) throws PersistentException {
		try {
			autopartsstore.AutoPartsStorePersistentManager.instance().saveObject(parts);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean delete(autopartsstore.Parts parts) throws PersistentException {
		try {
			autopartsstore.AutoPartsStorePersistentManager.instance().deleteObject(parts);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(autopartsstore.Parts parts)throws PersistentException {
		try {
			if (parts.getCategory() != null) {
				parts.getCategory().parts.remove(parts);
			}
			
			return delete(parts);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean deleteAndDissociate(autopartsstore.Parts parts, org.orm.PersistentSession session)throws PersistentException {
		try {
			if (parts.getCategory() != null) {
				parts.getCategory().parts.remove(parts);
			}
			
			try {
				session.delete(parts);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean refresh(autopartsstore.Parts parts) throws PersistentException {
		try {
			autopartsstore.AutoPartsStorePersistentManager.instance().getSession().refresh(parts);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static boolean evict(autopartsstore.Parts parts) throws PersistentException {
		try {
			autopartsstore.AutoPartsStorePersistentManager.instance().getSession().evict(parts);
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new PersistentException(e);
		}
	}
	
	public static Parts loadPartsByCriteria(PartsCriteria partsCriteria) {
		Parts[] partses = listPartsByCriteria(partsCriteria);
		if(partses == null || partses.length == 0) {
			return null;
		}
		return partses[0];
	}
	
	public static Parts[] listPartsByCriteria(PartsCriteria partsCriteria) {
		return partsCriteria.listParts();
	}
}
