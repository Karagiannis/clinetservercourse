/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package autopartsstore;

public class Parts {
	public Parts() {
	}
	
	private void this_setOwner(Object owner, int key) {
		if (key == autopartsstore.ORMConstants.KEY_PARTS_CATEGORY) {
			this.category = (autopartsstore.Category) owner;
		}
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public void setOwner(Object owner, int key) {
			this_setOwner(owner, key);
		}
		
	};
	
	private int ID;
	
	private autopartsstore.Category category;
	
	private String name;
	
	private Integer stockQTY;
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setName(String value) {
		this.name = value;
	}
	
	public String getName() {
		return name;
	}
	
	public void setStockQTY(int value) {
		setStockQTY(new Integer(value));
	}
	
	public void setStockQTY(Integer value) {
		this.stockQTY = value;
	}
	
	public Integer getStockQTY() {
		return stockQTY;
	}
	
	public void setCategory(autopartsstore.Category value) {
		if (category != null) {
			category.parts.remove(this);
		}
		if (value != null) {
			value.parts.add(this);
		}
	}
	
	public autopartsstore.Category getCategory() {
		return category;
	}
	
	/**
	 * This method is for internal use only.
	 */
	public void setORM_Category(autopartsstore.Category value) {
		this.category = value;
	}
	
	private autopartsstore.Category getORM_Category() {
		return category;
	}
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
