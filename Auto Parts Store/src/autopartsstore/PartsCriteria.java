/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package autopartsstore;

import org.hibernate.Criteria;
import org.orm.PersistentException;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class PartsCriteria extends AbstractORMCriteria {
	public final IntegerExpression ID;
	public final IntegerExpression categoryId;
	public final AssociationExpression category;
	public final StringExpression name;
	public final IntegerExpression stockQTY;
	
	public PartsCriteria(Criteria criteria) {
		super(criteria);
		ID = new IntegerExpression("ID", this);
		categoryId = new IntegerExpression("category.ID", this);
		category = new AssociationExpression("category", this);
		name = new StringExpression("name", this);
		stockQTY = new IntegerExpression("stockQTY", this);
	}
	
	public PartsCriteria(PersistentSession session) {
		this(session.createCriteria(Parts.class));
	}
	
	public PartsCriteria() throws PersistentException {
		this(autopartsstore.AutoPartsStorePersistentManager.instance().getSession());
	}
	
	public CategoryCriteria createCategoryCriteria() {
		return new CategoryCriteria(createCriteria("category"));
	}
	
	public Parts uniqueParts() {
		return (Parts) super.uniqueResult();
	}
	
	public Parts[] listParts() {
		java.util.List list = super.list();
		return (Parts[]) list.toArray(new Parts[list.size()]);
	}
}

