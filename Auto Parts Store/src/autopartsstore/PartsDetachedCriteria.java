/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package autopartsstore;

import java.util.List;
import org.hibernate.criterion.DetachedCriteria;
import org.orm.PersistentSession;
import org.orm.criteria.*;

public class PartsDetachedCriteria extends AbstractORMDetachedCriteria {
	public final IntegerExpression ID;
	public final IntegerExpression categoryId;
	public final AssociationExpression category;
	public final StringExpression name;
	public final IntegerExpression stockQTY;
	
	public PartsDetachedCriteria() {
		super(autopartsstore.Parts.class, autopartsstore.PartsCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		categoryId = new IntegerExpression("category.ID", this.getDetachedCriteria());
		category = new AssociationExpression("category", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		stockQTY = new IntegerExpression("stockQTY", this.getDetachedCriteria());
	}
	
	public PartsDetachedCriteria(DetachedCriteria aDetachedCriteria) {
		super(aDetachedCriteria, autopartsstore.PartsCriteria.class);
		ID = new IntegerExpression("ID", this.getDetachedCriteria());
		categoryId = new IntegerExpression("category.ID", this.getDetachedCriteria());
		category = new AssociationExpression("category", this.getDetachedCriteria());
		name = new StringExpression("name", this.getDetachedCriteria());
		stockQTY = new IntegerExpression("stockQTY", this.getDetachedCriteria());
	}
	
	public CategoryDetachedCriteria createCategoryCriteria() {
		return new CategoryDetachedCriteria(createCriteria("category"));
	}
	
	public Parts uniqueParts(PersistentSession session) {
		return (Parts) super.createExecutableCriteria(session).uniqueResult();
	}
	
	public Parts[] listParts(PersistentSession session) {
		List list = super.createExecutableCriteria(session).list();
		return (Parts[]) list.toArray(new Parts[list.size()]);
	}
}

