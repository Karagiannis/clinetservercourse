/**
 * "Visual Paradigm: DO NOT MODIFY THIS FILE!"
 * 
 * This is an automatic generated file. It will be regenerated every time 
 * you generate persistence class.
 * 
 * Modifying its content may cause the program not work, or your work may lost.
 */

/**
 * Licensee: 
 * License Type: Evaluation
 */
package autopartsstore;

public class Category {
	public Category() {
	}
	
	private java.util.Set this_getSet (int key) {
		if (key == autopartsstore.ORMConstants.KEY_CATEGORY_PARTS) {
			return ORM_parts;
		}
		
		return null;
	}
	
	org.orm.util.ORMAdapter _ormAdapter = new org.orm.util.AbstractORMAdapter() {
		public java.util.Set getSet(int key) {
			return this_getSet(key);
		}
		
	};
	
	private int ID;
	
	private String name;
	
	private java.util.Set ORM_parts = new java.util.HashSet();
	
	private void setID(int value) {
		this.ID = value;
	}
	
	public int getID() {
		return ID;
	}
	
	public int getORMID() {
		return getID();
	}
	
	public void setName(String value) {
		this.name = value;
	}
	
	public String getName() {
		return name;
	}
	
	private void setORM_Parts(java.util.Set value) {
		this.ORM_parts = value;
	}
	
	private java.util.Set getORM_Parts() {
		return ORM_parts;
	}
	
	public final autopartsstore.PartsSetCollection parts = new autopartsstore.PartsSetCollection(this, _ormAdapter, autopartsstore.ORMConstants.KEY_CATEGORY_PARTS, autopartsstore.ORMConstants.KEY_PARTS_CATEGORY, autopartsstore.ORMConstants.KEY_MUL_ONE_TO_MANY);
	
	public String toString() {
		return String.valueOf(getID());
	}
	
}
