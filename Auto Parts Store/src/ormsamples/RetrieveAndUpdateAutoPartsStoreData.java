/**
 * Licensee: 
 * License Type: Evaluation
 */
package ormsamples;

import org.orm.*;
public class RetrieveAndUpdateAutoPartsStoreData {
	public void retrieveAndUpdateTestData() throws PersistentException {
		PersistentTransaction t = autopartsstore.AutoPartsStorePersistentManager.instance().getSession().beginTransaction();
		try {
			autopartsstore.Category lautopartsstoreCategory = autopartsstore.CategoryDAO.loadCategoryByQuery(null, null);
			// Update the properties of the persistent object
			autopartsstore.CategoryDAO.save(lautopartsstoreCategory);
			autopartsstore.Parts lautopartsstoreParts = autopartsstore.PartsDAO.loadPartsByQuery(null, null);
			// Update the properties of the persistent object
			autopartsstore.PartsDAO.save(lautopartsstoreParts);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public void retrieveByCriteria() throws PersistentException {
		System.out.println("Retrieving Category by CategoryCriteria");
		autopartsstore.CategoryCriteria lautopartsstoreCategoryCriteria = new autopartsstore.CategoryCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lautopartsstoreCategoryCriteria.ID.eq();
		System.out.println(lautopartsstoreCategoryCriteria.uniqueCategory());
		
		System.out.println("Retrieving Parts by PartsCriteria");
		autopartsstore.PartsCriteria lautopartsstorePartsCriteria = new autopartsstore.PartsCriteria();
		// Please uncomment the follow line and fill in parameter(s)
		//lautopartsstorePartsCriteria.ID.eq();
		System.out.println(lautopartsstorePartsCriteria.uniqueParts());
		
	}
	
	
	public static void main(String[] args) {
		try {
			RetrieveAndUpdateAutoPartsStoreData retrieveAndUpdateAutoPartsStoreData = new RetrieveAndUpdateAutoPartsStoreData();
			try {
				retrieveAndUpdateAutoPartsStoreData.retrieveAndUpdateTestData();
				//retrieveAndUpdateAutoPartsStoreData.retrieveByCriteria();
			}
			finally {
				autopartsstore.AutoPartsStorePersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
