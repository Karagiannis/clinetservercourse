/**
 * Licensee: 
 * License Type: Evaluation
 */
package ormsamples;

import org.orm.*;
public class DeleteAutoPartsStoreData {
	public void deleteTestData() throws PersistentException {
		PersistentTransaction t = autopartsstore.AutoPartsStorePersistentManager.instance().getSession().beginTransaction();
		try {
			autopartsstore.Category lautopartsstoreCategory = autopartsstore.CategoryDAO.loadCategoryByQuery(null, null);
			// Delete the persistent object
			autopartsstore.CategoryDAO.delete(lautopartsstoreCategory);
			autopartsstore.Parts lautopartsstoreParts = autopartsstore.PartsDAO.loadPartsByQuery(null, null);
			// Delete the persistent object
			autopartsstore.PartsDAO.delete(lautopartsstoreParts);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
		
	}
	
	public static void main(String[] args) {
		try {
			DeleteAutoPartsStoreData deleteAutoPartsStoreData = new DeleteAutoPartsStoreData();
			try {
				deleteAutoPartsStoreData.deleteTestData();
			}
			finally {
				autopartsstore.AutoPartsStorePersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
