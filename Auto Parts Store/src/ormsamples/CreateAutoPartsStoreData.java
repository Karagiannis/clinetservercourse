/**
 * Licensee: 
 * License Type: Evaluation
 */
package ormsamples;

import org.orm.*;
public class CreateAutoPartsStoreData {
	public void createTestData() throws PersistentException {
		// Insert sample data
		PersistentSession session = autopartsstore.AutoPartsStorePersistentManager.instance().getSession();
//		PersistentTransaction transaction = session.beginTransaction();
//		try {
//			session.doWork(new org.hibernate.jdbc.Work() {
//				public void execute(java.sql.Connection conn) throws java.sql.SQLException {
//					java.sql.Statement statement = conn.createStatement();
//					statement.executeUpdate("INSERT INTO Category(ID, name) VALUES (1, 'Brake')");
//					statement.executeUpdate("INSERT INTO Category(ID, name) VALUES (2, 'Engine')");
//					statement.executeUpdate("INSERT INTO Parts(ID, CategoryID, name, stockQTY) VALUES (1, 1, 'Brake Pads', 250)");
//					statement.executeUpdate("INSERT INTO Parts(ID, CategoryID, name, stockQTY) VALUES (2, 2, 'Oil Filter', 500)");
//					statement.close();
//				}
//			});
//			transaction.commit();
//		}
//		catch (Exception e) {
//			try {
//				transaction.rollback();
//			}
//			catch (PersistentException e1) {
//				e.printStackTrace();
//			}
//			e.printStackTrace();
//		}
//		
		PersistentTransaction t = autopartsstore.AutoPartsStorePersistentManager.instance().getSession().beginTransaction();
		try {
			autopartsstore.Category lautopartsstoreCategory = autopartsstore.CategoryDAO.createCategory();
			lautopartsstoreCategory.setName("Air-Con");
                         
                        autopartsstore.Parts lautopartsstoreParts = autopartsstore.PartsDAO.createParts();
                        lautopartsstoreParts.setName("Ac-filter");
                        lautopartsstoreParts.setStockQTY(100);
                        
                        lautopartsstoreCategory.parts.add(lautopartsstoreParts);

			autopartsstore.CategoryDAO.save(lautopartsstoreCategory);
			autopartsstore.PartsDAO.save(lautopartsstoreParts);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
		}
                
                System.out.println("Record saved");
		
	}
	
	public static void main(String[] args) {
		try {
			CreateAutoPartsStoreData createAutoPartsStoreData = new CreateAutoPartsStoreData();
			try {
				createAutoPartsStoreData.createTestData();
			}
			finally {
				autopartsstore.AutoPartsStorePersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
