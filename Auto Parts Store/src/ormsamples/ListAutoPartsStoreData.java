/**
 * Licensee: 
 * License Type: Evaluation
 */
package ormsamples;

import autopartsstore.Parts;
import org.orm.*;
public class ListAutoPartsStoreData {
	private static final int ROW_COUNT = 100;
	
	public void listTestData() throws PersistentException {
		System.out.println("Listing Category...");
		autopartsstore.Category[] autopartsstoreCategorys = autopartsstore.CategoryDAO.listCategoryByQuery(null, null);
		int length = Math.min(autopartsstoreCategorys.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(autopartsstoreCategorys[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
		System.out.println("Listing Parts...");
		autopartsstore.Parts[] autopartsstorePartses = autopartsstore.PartsDAO.listPartsByQuery(null, null);
		length = Math.min(autopartsstorePartses.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			System.out.println(autopartsstorePartses[i]);
		}
		System.out.println(length + " record(s) retrieved.");
		
	}
	
	public void listByCriteria() throws PersistentException {
		System.out.println("Listing Category by Criteria...");
		autopartsstore.CategoryCriteria lautopartsstoreCategoryCriteria = new autopartsstore.CategoryCriteria();
		// Please uncomment the follow line and fill in parameter(s) 
		//lautopartsstoreCategoryCriteria.ID.eq();
		lautopartsstoreCategoryCriteria.setMaxResults(ROW_COUNT);
		autopartsstore.Category[] autopartsstoreCategorys = lautopartsstoreCategoryCriteria.listCategory();
		int length =autopartsstoreCategorys== null ? 0 : Math.min(autopartsstoreCategorys.length, ROW_COUNT); 
		for (int i = 0; i < length; i++) {
			 System.out.println(autopartsstoreCategorys[i]);
		}
//		System.out.println(length + " Category record(s) retrieved."); 
//		
//		System.out.println("Listing Parts by Criteria...");
//		autopartsstore.PartsCriteria lautopartsstorePartsCriteria = new autopartsstore.PartsCriteria();
//		// Please uncomment the follow line and fill in parameter(s) 
//		//lautopartsstorePartsCriteria.ID.eq();
//		lautopartsstorePartsCriteria.setMaxResults(ROW_COUNT);
//		autopartsstore.Parts[] autopartsstorePartses = lautopartsstorePartsCriteria.listParts();
//		length =autopartsstorePartses== null ? 0 : Math.min(autopartsstorePartses.length, ROW_COUNT); 
//		for (int i = 0; i < length; i++) {
//			 System.out.println(autopartsstorePartses[i]);
//		}
//		System.out.println(length + " Parts record(s) retrieved."); 
		length = Math.min(autopartsstoreCategorys.length, ROW_COUNT);
                
                for (int i = 0; i < length; i++) {
                        System.out.println("Category: ");
                        System.out.println("     " + autopartsstoreCategorys[i].getName());
                        Parts[] parts = autopartsstoreCategorys[i].parts.toArray(); 
                                 for (int j = 0; j < parts.length; j++)                            
                                    System.out.println("Parts: " + parts[j].getName() + ", QTY: " + parts[j].getStockQTY());    
                        
                }
                
}
	
	public static void main(String[] args) {
		try {
			ListAutoPartsStoreData listAutoPartsStoreData = new ListAutoPartsStoreData();
			try {
				//listAutoPartsStoreData.listTestData();
				listAutoPartsStoreData.listByCriteria();
			}
			finally {
				autopartsstore.AutoPartsStorePersistentManager.instance().disposePersistentManager();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
