CREATE TABLE Category (ID int(11) NOT NULL AUTO_INCREMENT, name varchar(50), PRIMARY KEY (ID));
CREATE TABLE Parts (ID int(11) NOT NULL AUTO_INCREMENT, CategoryID int(11) NOT NULL, name varchar(100), stockQTY int(11), PRIMARY KEY (ID));
ALTER TABLE Parts ADD INDEX FKParts929914 (CategoryID), ADD CONSTRAINT FKParts929914 FOREIGN KEY (CategoryID) REFERENCES Category (ID);
INSERT INTO Category(ID, name) VALUES (1, 'Brake');
INSERT INTO Category(ID, name) VALUES (2, 'Engine');
INSERT INTO Parts(ID, CategoryID, name, stockQTY) VALUES (1, 1, 'Brake Pads', 250);
INSERT INTO Parts(ID, CategoryID, name, stockQTY) VALUES (2, 2, 'Oil Filter', 500);
