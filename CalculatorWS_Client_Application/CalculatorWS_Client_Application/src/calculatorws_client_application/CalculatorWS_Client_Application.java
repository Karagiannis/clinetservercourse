/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package calculatorws_client_application;

/**
 *
 * @author Lasse
 */
public class CalculatorWS_Client_Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
                int i = 3;
                int j = 4;
                int result = add(i, j);
                System.out.println("Result = " + result);
            } catch (Exception ex) {
                System.out.println("Exception: " + ex);
            }
    }

    private static Integer add(int i, int j) {
        org.me.calculator.CalculatorWs_Service service = new org.me.calculator.CalculatorWs_Service();
        org.me.calculator.CalculatorWs port = service.getCalculatorWsPort();
        return port.add(i, j);
    }
    
}
